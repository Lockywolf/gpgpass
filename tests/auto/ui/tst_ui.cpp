/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#include "pass.h"
#include "passwordconfiguration.h"
#include "passworddialog.h"
#include <QCoreApplication>
#include <QtTest>

/**
 * @brief The tst_ui class is our first unit test
 */
class tst_ui : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void contentRemainsSame();
};

/**
 * @brief tst_ui::contentRemainsSame test that content set with
 * PasswordDialog::setPassword is repeated when calling
 * PasswordDialog::getPassword.
 */
void tst_ui::contentRemainsSame()
{
    auto pass = std::make_unique<Pass>();
    QScopedPointer<PasswordDialog> d(new PasswordDialog(*pass, PasswordConfiguration{}, nullptr));
    d->setTemplate(QString{}, false);
    QString input = QStringLiteral("pw\n");
    d->setPass(input);
    QCOMPARE(d->getPassword(), input);

    d.reset(new PasswordDialog(*pass, PasswordConfiguration{}, nullptr));
    input = QStringLiteral("pw\nname: value\n");
    d->setPass(input);
    QCOMPARE(d->getPassword(), input);

    d.reset(new PasswordDialog(*pass, PasswordConfiguration{}, nullptr));
    d->setTemplate(QStringLiteral("name"), false);
    d->setPass(input);
    QCOMPARE(d->getPassword(), input);

    d.reset(new PasswordDialog(*pass, PasswordConfiguration{}, nullptr));
    d->setTemplate(QStringLiteral("name"), true);
    d->setPass(input);
    QCOMPARE(d->getPassword(), input);

    d.reset(new PasswordDialog(*pass, PasswordConfiguration{}, nullptr));
    d->setTemplate(QString(), false);
    d->templateAll(true);
    d->setPass(input);
    QCOMPARE(d->getPassword(), input);

    d.reset(new PasswordDialog(*pass, PasswordConfiguration{}, nullptr));
    d->setTemplate(QString(), true);
    d->templateAll(true);
    d->setPass(input);
    QCOMPARE(d->getPassword(), input);

    d.reset(new PasswordDialog(*pass, PasswordConfiguration{}, nullptr));
    d->setTemplate(QStringLiteral("name"), true);
    d->templateAll(true);
    d->setPass(input);
    QCOMPARE(d->getPassword(), input);
}

QTEST_MAIN(tst_ui)
#include "tst_ui.moc"
