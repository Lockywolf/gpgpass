/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#include "filecontent.h"
#include "util.h"
#include <QCoreApplication>
#include <QList>
#include <QtTest>

/**
 * @brief The tst_util class is our first unit test
 */
class tst_util : public QObject
{
    Q_OBJECT

public:
    tst_util();
    ~tst_util() override;

public Q_SLOTS:
    void init();
    void cleanup();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void normalizeFolderPath();
    void fileContent();
};

bool operator==(const NamedValue &a, const NamedValue &b)
{
    return a.name == b.name && a.value == b.value;
}

/**
 * @brief tst_util::tst_util basic constructor
 */
tst_util::tst_util() = default;

/**
 * @brief tst_util::~tst_util basic destructor
 */
tst_util::~tst_util() = default;

/**
 * @brief tst_util::init unit test init method
 */
void tst_util::init()
{
}

/**
 * @brief tst_util::cleanup unit test cleanup method
 */
void tst_util::cleanup()
{
}

/**
 * @brief tst_util::initTestCase test case init method
 */
void tst_util::initTestCase()
{
}

/**
 * @brief tst_util::cleanupTestCase test case cleanup method
 */
void tst_util::cleanupTestCase()
{
}

/**
 * @brief tst_util::normalizeFolderPath test to check correct working
 * of Util::normalizeFolderPath the paths should always end with a slash
 */
void tst_util::normalizeFolderPath()
{
    QCOMPARE(Util::normalizeFolderPath(QStringLiteral("test")), QStringLiteral("test/"));
    QCOMPARE(Util::normalizeFolderPath(QStringLiteral("test/")), QStringLiteral("test/"));
}

void tst_util::fileContent()
{
    NamedValue key = {QStringLiteral("key"), QStringLiteral("val")};
    NamedValue key2 = {QStringLiteral("key2"), QStringLiteral("val2")};
    QString password = QStringLiteral("password");

    FileContent fc = FileContent::parse(QStringLiteral("password\n"), {}, false);
    QCOMPARE(fc.getPassword(), password);
    QCOMPARE(fc.getNamedValues(), {});
    QCOMPARE(fc.getRemainingData(), QString());

    fc = FileContent::parse(QStringLiteral("password"), {}, false);
    QCOMPARE(fc.getPassword(), password);
    QCOMPARE(fc.getNamedValues(), {});
    QCOMPARE(fc.getRemainingData(), QString());

    fc = FileContent::parse(QStringLiteral("password\nfoobar\n"), {}, false);
    QCOMPARE(fc.getPassword(), password);
    QCOMPARE(fc.getNamedValues(), {});
    QCOMPARE(fc.getRemainingData(), QStringLiteral("foobar\n"));

    fc = FileContent::parse(QStringLiteral("password\nkey: val\nkey2: val2"), {QStringLiteral("key2")}, false);
    QCOMPARE(fc.getPassword(), password);
    QCOMPARE(fc.getNamedValues(), QList<NamedValue>{key2});
    QCOMPARE(fc.getRemainingData(), QStringLiteral("key: val"));

    fc = FileContent::parse(QStringLiteral("password\nkey: val\nkey2: val2"), {QStringLiteral("key2")}, true);
    QCOMPARE(fc.getPassword(), password);
    QCOMPARE(fc.getNamedValues(), NamedValues({key, key2}));
    QCOMPARE(fc.getRemainingData(), QString());
}

QTEST_MAIN(tst_util)
#include "tst_util.moc"
