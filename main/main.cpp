/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/

#include "firsttimedialog.h"
#include "mainwindow.h"

#include <QApplication>
#include <QDir>
#include <QTranslator>
#include <QtWidgets>

#include <gpgpass_version.h>
#include <KIconLoader>
#include <KColorSchemeManager>
#include <KLocalizedString>

/**
 * @brief main
 * @param argc
 * @param argv
 * @return
 */
#include <sys/stat.h>
int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
#endif
// allow darkmode
#if defined(Q_OS_WIN)
    if (!qEnvironmentVariableIsSet("QT_QPA_PLATFORM")) {
        qputenv("QT_QPA_PLATFORM", "windows:darkmode=1");
    }
#endif

    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("gpgpass");

    Q_INIT_RESOURCE(resources);

    QCoreApplication::setOrganizationName(QStringLiteral("GnuPG"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("gnupg.org"));
    QCoreApplication::setApplicationName(QStringLiteral("GnuPGPass"));
    QCoreApplication::setApplicationVersion(QString::fromUtf8(GPGPASS_VERSION_STRING));

#ifdef Q_OS_WINDOWS
    QApplication::setWindowIcon(QIcon(QStringLiteral(":/artwork/64-gpgpass.png")));
#else
    QApplication::setWindowIcon(QIcon::fromTheme(QStringLiteral("org.gnupg.gpgpass")));
#endif
    QGuiApplication::setDesktopFileName(QStringLiteral("org.gnupg.gpgpass"));
    MainWindow w;

    // ensure KIconThemes is loaded for rcc icons
    KIconLoader::global()->hasIcon(QString{});

    KColorSchemeManager m;

    FirstTimeDialog d(&w);

    QSettings s;

    if (!s.value(QStringLiteral("setup"), false).toBool()) {
        if (d.wouldDoSomething()) {
            d.show();
        } else {
            d.doInitialSettings();
            w.show();
        }
    } else {
        w.show();
    }

    return app.exec();
}
