/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2016-2017 tezeb <tezeb+github@outoftheblue.pl>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2019Maciej S. Szmigiero <mail@maciej.szmigiero.name>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include "storemodel.h"

#include <QFileSystemModel>
#include <QItemSelectionModel>
#include <QMainWindow>
#include <QProcess>
#include <QTimer>

#ifdef __APPLE__
// http://doc.qt.io/qt-5/qkeysequence.html#qt_set_sequence_auto_mnemonic
void qt_set_sequence_auto_mnemonic(bool b);
#endif

namespace Ui
{
class MainWindow;
}

/*!
    \class MainWindow
    \brief The MainWindow class does way too much, not only is it a switchboard,
    configuration handler and more, it's also the process-manager.

    This class could really do with an overhaul.
 */
class QComboBox;
class ClipboardHelper;
class Pass;
class KMessageWidget;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void restoreWindow();
    void userDialog(QString = {});
    void config();

    void setUiElementsEnabled(bool state);

    const QModelIndex getCurrentTreeViewIndex();

    void setVisible(bool visible) override;

protected:
    void closeEvent(QCloseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void changeEvent(QEvent *event) override;
    bool eventFilter(QObject *obj, QEvent *event) override;

public Q_SLOTS:
    void deselect();

    void critical(const QString&, const QString&);

    void passShowHandler(const QString &);

    void selectTreeItem(const QModelIndex &index);

    void startReencryptPath();
    void endReencryptPath();

private Q_SLOTS:
    void addPassword();
    void addFolder();
    void onEdit();
    void onDelete();
    void onUsers();
    void onConfig();
    void editTreeItem(const QModelIndex &index);
    void clearPanel();
    void filterList(const QString &arg1);
    void selectFromSearch();
    void selectProfile(QString);
    void showContextMenu(const QPoint &pos);
    void renameFolder();
    void editPassword(const QString &);
    void renamePassword();
    void focusInput();
    void onTimeoutSearch();
    void verifyInitialized();

private:
    std::unique_ptr<Pass> m_pass;
    ClipboardHelper *m_clipboardHelper;
    QScopedPointer<Ui::MainWindow> ui;
    QFileSystemModel model;
    StoreModel proxyModel;
    QScopedPointer<QItemSelectionModel> selectionModel;
    QTimer clearPanelTimer, searchTimer;
    KMessageWidget* m_notInitialized;
    KMessageWidget* m_errorMessage;
    QAction* m_profiles;
    QComboBox* m_profileBox;
    bool firstShow = true;

    void initToolBarButtons();
    void initStatusBar();

    void showRemainingHtml(const QString &text);
    void updateText();
    void selectFirstFile();
    QModelIndex firstFile(QModelIndex parentIndex);
    void setPassword(QString, bool isNew = true);

    void updateProfileBox();
    void initTrayIcon();
    void destroyTrayIcon();
    void clearTemplateWidgets();
    void reencryptPath(QString dir);
    void addToGridLayout(const QString &field, const QString &value);
};

#endif // MAINWINDOW_H_
