/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2019 Maciej S. Szmigiero <mail@maciej.szmigiero.name>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef STOREMODEL_H_
#define STOREMODEL_H_

#include <QSortFilterProxyModel>

/*!
    \class StoreModel
    \brief The QSortFilterProxyModel for handling filesystem searches.
 */
class QFileSystemModel;
class Pass;
class StoreModel : public QSortFilterProxyModel
{
    Q_OBJECT

private:
    QFileSystemModel *fs() const;
    Pass &m_pass;

public:
    explicit StoreModel(Pass &pass);

    QVariant data(const QModelIndex &index, int role) const override;
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;

    // QAbstractItemModel interface
public:
    Qt::DropActions supportedDropActions() const override;
    Qt::DropActions supportedDragActions() const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QStringList mimeTypes() const override;
    QMimeData *mimeData(const QModelIndexList &indexes) const override;
    bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) const override;
    bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
};
/*!
    \struct dragAndDropInfo
    \brief holds values to share beetween drag and drop on the passwordstorage
   view
 */
struct dragAndDropInfoPasswordStore {
    bool isDir = false;
    bool isFile = false;
    QString path;
};

#endif // STOREMODEL_H_
