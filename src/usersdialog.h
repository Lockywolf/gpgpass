/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef USERSDIALOG_H_
#define USERSDIALOG_H_

#include "userinfo.h"

#include <QDialog>
#include <QList>

class Ui_UsersDialog;
class Ui_UsersWidget;
class Pass;

class QCloseEvent;
class QKeyEvent;
class QListWidgetItem;

struct UsersWidgetData {
    explicit UsersWidgetData(Pass &pass)
        : m_pass(pass), ui(nullptr)
    {
    }
    QList<UserInfo> m_userList;
    QString m_dir;
    Pass &m_pass;
    Ui_UsersWidget *ui;

    void generateUserList();
    void init();
    void setUi(Ui_UsersWidget *ui);
    void itemChange(QListWidgetItem *item);
    void populateList();
};

/*!
    \class UsersDialog
    \brief Handles listing and editing of GPG users.

    Selection of whom to encrypt to.
 */
class UsersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UsersDialog(const QString& dir, Pass &pass, QWidget *parent = nullptr);
    ~UsersDialog();

public Q_SLOTS:
    void accept() override;

private:
    std::unique_ptr<Ui_UsersDialog> dialogUi;
    std::unique_ptr<Ui_UsersWidget> ui;
    UsersWidgetData d;
};

#endif // USERSDIALOG_H_
