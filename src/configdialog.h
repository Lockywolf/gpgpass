/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef CONFIGDIALOG_H_
#define CONFIGDIALOG_H_

#include "passwordconfiguration.h"
#include <QDialog>

namespace Ui
{
struct UserInfo;

class ConfigDialog;
} // namespace Ui

/*!
    \class ConfigDialog
    \brief The ConfigDialog handles the configuration interface.

    This class should also take the handling from the MainWindow class.
*/
class MainWindow;
class QCloseEvent;
class QTableWidgetItem;
class ConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConfigDialog(MainWindow *parent);
    ~ConfigDialog();

private Q_SLOTS:
    void on_accepted();
    void selectStoreFolder();
    void addProfile();
    void deleteSelectedProfile();
    void toggleAutoClearPanelSubentries(bool enable);
    void toggleAutoClearSubentries(bool enable);
    void toggleTemplateSubentries(bool enable);
    void validateNewProfile();

private:
    void useAutoclear(bool useAutoclear);
    void useAutoclearPanel(bool useAutoclearPanel);
    QHash<QString, QString> getProfiles();
    void setPasswordConfiguration(const PasswordConfiguration &config);
    PasswordConfiguration getPasswordConfiguration();
    void useTemplate(bool useTemplate);
    QScopedPointer<Ui::ConfigDialog> ui;

    void setProfiles(const QHash<QString, QString>&, const QString&);

    QString selectFolder();

    bool checkNewProfileData();

    MainWindow *mainWindow;
};

#endif // CONFIGDIALOG_H_
