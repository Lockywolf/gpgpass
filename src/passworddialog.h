/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef PASSWORDDIALOG_H_
#define PASSWORDDIALOG_H_

#include "passwordconfiguration.h"
#include <QDialog>

namespace Ui
{
class PasswordDialog;
}

class QLineEdit;
class QWidget;
class Pass;

/*!
    \class PasswordDialog
    \brief PasswordDialog Handles the inserting and editing of passwords.

    Includes templated views.
 */
class PasswordDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PasswordDialog(Pass &pass, const PasswordConfiguration &passConfig, QWidget *parent = nullptr);
    PasswordDialog(Pass &pass, const QString &file, const bool &isNew, QWidget *parent = nullptr);
    ~PasswordDialog();

    /*! Sets content in the password field in the interface.
        \param password the password as a QString
        \sa getPassword
     */
    void setPassword(const QString& password);

    /*! Returns the password as set in the password field in the interface.
        \return password as a QString
        \sa setPassword
     */
    QString getPassword();

    /*! Sets content in the template for the interface.
        \param rawFields is the template as a QString
        \param useTemplate whether the template is used
     */
    void setTemplate(const QString& rawFields, bool useTemplate);

    void templateAll(bool templateAll);
    void setLength(int l);
    void setPasswordCharTemplate(int t);

public Q_SLOTS:
    void setPass(const QString &output);

private Q_SLOTS:
    void showPasswordChanged(int arg1);
    void createPassword();
    void dialogAccepted();
    void dialogCancelled();

private:
    std::unique_ptr<Ui::PasswordDialog> ui;
    Pass &m_pass;
    PasswordConfiguration m_passConfig;
    QStringList m_fields;
    QString m_file;
    bool m_templating;
    bool m_allFields;
    bool m_isNew;
    QList<QLineEdit *> templateLines;
    QList<QLineEdit *> otherLines;
};

#endif // PASSWORDDIALOG_H_
