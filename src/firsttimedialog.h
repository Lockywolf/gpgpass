/*
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef FIRSTTIMEDIALOG_H
#define FIRSTTIMEDIALOG_H

#include "usersdialog.h"
#include <QGpgME/Protocol>
#include <QWizard>
#include <userinfo.h>

class DialogState : public QObject
{
    Q_OBJECT
public:
    QList<UserInfo> privateKeys() const;
    QGpgME::Protocol *protocol = QGpgME::openpgp();
};

class FirstTimeDialog : public QWizard
{
    enum Pages { Intro, KeyGen, Done };
    Q_OBJECT
public:
    FirstTimeDialog(QWidget *mainWindow);
    int nextId() const override;
    void done(int i) override;
    bool wouldDoSomething() const;
    void doInitialSettings();

private:
    QWidget *m_mainWindow;
    DialogState m_state;
};

class IntroPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit IntroPage(DialogState &s);
    bool isComplete() const override;

private Q_SLOTS:
    void refresh();
private:
    DialogState &m_state;
    QString m_kleoPath;
    bool m_complete = false;
    QWidget* m_noKeysContainer;
    QWidget* m_keysContainer;
};

#endif // FIRSTTIMEDIALOG_H
