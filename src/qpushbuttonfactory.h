/*
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef QPUSHBUTTONFACTORY_H_
#define QPUSHBUTTONFACTORY_H_

#include <QLineEdit>
#include <QPushButton>
#include <memory>

template<typename ReceiverObject, typename Callable>
std::unique_ptr<QPushButton> createPushButton(const QIcon &icon, const QString& accessibleName, ReceiverObject *onClickedReceiver, Callable target)
{
    static_assert(std::is_base_of<QObject, ReceiverObject>::value, "Receiver should inherit from QObject");
    auto button = std::make_unique<QPushButton>(icon, QString{});
    button->setAccessibleName(accessibleName);
    button->setToolTip(accessibleName);
    button->connect(button.get(), &QPushButton::clicked, onClickedReceiver, target);
    return button;
}

#endif // QPUSHBUTTONFACTORY_H_
