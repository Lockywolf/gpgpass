/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef CLIPBOARDHELPER_H
#define CLIPBOARDHELPER_H

#include <QObject>
#include <QTimer>

class QMainWindow;

/**
 * Helper for Clipboard interactions
 *
 * This should be the only file where clipboard interactions
 * should happen. This ensures we can attempt to clear it after some
 * interval to try help avoid leaking passwords thru the clipboard
 */
class ClipboardHelper : public QObject
{
    Q_OBJECT
public:
    explicit ClipboardHelper(QMainWindow *mainWindow);
    ~ClipboardHelper();

    void setClippedText(const QString &);
    void clearClippedText();
    void setClipboardTimer();

private:
    QMainWindow *m_mainWindow;
    QTimer clearClipboardTimer;
    QString clippedText;

public Q_SLOTS:
    void clearClipboard();
    void copyTextToClipboard(const QString &text);
};

#endif // CLIPBOARDHELPER_H
