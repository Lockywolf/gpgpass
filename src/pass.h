/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2017 Jason A. Donenfeld <Jason@zx2c4.com>
    SPDX-FileCopyrightText: 2020 Charlie Waters <cawiii@me.com>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef PASS_H
#define PASS_H

#include "userinfo.h"

#include <QGpgME/Protocol>
#include <QQueue>
#include <QString>
#include <cassert>
#include <map>

/*!
    \class Pass
    \brief Acts as an abstraction for pass or pass imitation
*/
class Pass final : public QObject
{
    Q_OBJECT

public:
    Pass();

    void Show(const QString& file);
    void Insert(const QString& file, const QString& value);
    void Remove(const QString& file, bool isDir);
    void Move(const QString& srcDir, const QString& dest, const bool force = false);
    void Copy(const QString& srcDir, const QString& dest, const bool force = false);
    void Init(const QString &path, const QList<UserInfo> &users);
    QString Generate_b(unsigned int length, const QString &charset);

    QList<UserInfo> listKeys(const QStringList& keystrings, bool secret = false);
    QList<UserInfo> listKeys(const QString& keystring = QString{}, bool secret = false);
    static QStringList getRecipientList(const QString& for_file);

private:
    QGpgME::Protocol *protocol = QGpgME::openpgp();
    QString generateRandomPassword(const QString &charset, unsigned int length);
    quint32 boundedRandom(quint32 bound);

    void reencryptPath(const QString &dir);
Q_SIGNALS:
    void startReencryptPath();
    void endReencryptPath();
    void errorString(const QString &err);
    void critical(const QString&, const QString&);

    // messages are provided via errorString
    void decryptionError();

    void finishedShow(const QString &plainTextPassword);
    void finishedInsert();
};

#endif // PASS_H
