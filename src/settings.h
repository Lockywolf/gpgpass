/*
    SPDX-FileCopyrightText: 2014-2023 Anne Jan Brouwer <brouwer@annejan.com>
    SPDX-FileCopyrightText: 2018 Lukas Vogel <lukedirtwalker@gmail.com>
    SPDX-FileCopyrightText: 2018 Claudio Maradonna <penguyman@stronzi.org>
    SPDX-FileCopyrightText: 2022 Tobias Leupold <tl@l3u.de>
    SPDX-FileCopyrightText: 2023 g10 Code GmbH
    SPDX-FileContributor: Sune Stolborg Vuorela <sune@vuorela.dk>

    SPDX-License-Identifier: GPL-3.0-or-later
*/
#ifndef SETTINGS_H
#define SETTINGS_H

#include "passwordconfiguration.h"

#include <QByteArray>
#include <QHash>
#include <QPoint>
#include <QSettings>
#include <QSize>
#include <QVariant>

/*!
    \class Settings
    \brief Singleton that stores settings, saves and loads config
*/
class Settings : public QSettings
{
private:
    Settings(const QString &filename, const QSettings::Format format)
        : QSettings(filename, format)
    {
    }
    explicit Settings()
        : QSettings()
    {
    }

    static bool initialized;
    static Settings *m_instance;
    static Settings *getInstance();

public:
    static QString getVersion(const QString &defaultValue = QVariant().toString());
    static void setVersion(const QString &version);

    static QByteArray getGeometry(const QByteArray &defaultValue = QVariant().toByteArray());
    static void setGeometry(const QByteArray &geometry);

    static QByteArray getSavestate(const QByteArray &defaultValue = QVariant().toByteArray());
    static void setSavestate(const QByteArray &saveState);

    static QPoint getPos(const QPoint &defaultValue = QVariant().toPoint());
    static void setPos(const QPoint &pos);

    static QSize getSize(const QSize &defaultValue = QVariant().toSize());
    static void setSize(const QSize &size);

    static bool isUseAutoclear(const bool &defaultValue = QVariant().toBool());
    static void setUseAutoclear(const bool &useAutoclear);

    static int getAutoclearSeconds(const int &defaultValue = QVariant().toInt());
    static void setAutoclearSeconds(const int &autoClearSeconds);

    static bool isUseAutoclearPanel(const bool &defaultValue = QVariant().toBool());
    static void setUseAutoclearPanel(const bool &useAutoclearPanel);

    static int getAutoclearPanelSeconds(const int &defaultValue = QVariant().toInt());
    static void setAutoclearPanelSeconds(const int &autoClearPanelSeconds);

    static bool isHideContent(const bool &defaultValue = QVariant().toBool());
    static void setHideContent(const bool &hideContent);

    static bool isDisplayAsIs(const bool &defaultValue = QVariant().toBool());
    static void setDisplayAsIs(const bool &displayAsIs);

    static bool isNoLineWrapping(const bool &defaultValue = QVariant().toBool());
    static void setNoLineWrapping(const bool &noLineWrapping);

    static QString getPassStore(const QString &defaultValue = QVariant().toString());
    static void setPassStore(const QString &passStore);

    static QString getProfile(const QString &defaultValue = QVariant().toString());
    static void setProfile(const QString &profile);

    static PasswordConfiguration getPasswordConfiguration();
    static void setPasswordConfiguration(const PasswordConfiguration &config);
    static void setPasswordLength(const int &passwordLength);
    static void setPasswordCharsselection(const int &passwordCharsselection);
    static void setPasswordChars(const QString &passwordChars);

    static bool isUseTrayIcon(const bool &defaultValue = QVariant().toBool());
    static void setUseTrayIcon(const bool &useTrayIcon);

    static bool isHideOnClose(const bool &defaultValue = QVariant().toBool());
    static void setHideOnClose(const bool &hideOnClose);

    static bool isStartMinimized(const bool &defaultValue = QVariant().toBool());
    static void setStartMinimized(const bool &startMinimized);

    static QString getPassTemplate(const QString &defaultValue = QVariant().toString());
    static void setPassTemplate(const QString &passTemplate);

    static bool isUseTemplate(const bool &defaultValue = QVariant().toBool());
    static void setUseTemplate(const bool &useTemplate);

    static bool isTemplateAllFields(const bool &defaultValue = QVariant().toBool());
    static void setTemplateAllFields(const bool &templateAllFields);

    static QHash<QString, QString> getProfiles();
    static void setProfiles(const QHash<QString, QString> &profiles);
};

#endif // SETTINGS_H
